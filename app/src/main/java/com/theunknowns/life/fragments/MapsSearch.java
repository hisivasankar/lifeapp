package com.theunknowns.life.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.theunknowns.life.activity.FloatingDonorProfile;
import com.theunknowns.life.settings.LifeAppRuntime;
import com.theunknowns.life.util.NetworkOperation;
import com.theunknowns.life.util.StaticStrings;
import com.theunknows.life.life.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class MapsSearch extends Fragment implements View.OnClickListener {

    public static final String LOG_TAG = MapsSearch.class.getSimpleName();
    private GoogleMap mMap;
    private LocationManager locationManager;
    private HashMap<String, JSONObject> donorCollection;
    private JSONObject dummyUser;

    private Context mContext;
    private LifeAppRuntime appRuntime;

    private Button mBtn_LocateMe;
    private Spinner spinner_BloodGroup;

    public static MapsSearch newInstance(String param1, String param2) {
        MapsSearch fragment = new MapsSearch();
        return fragment;
    }

    public MapsSearch() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void showGPSPromptWindow() {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getActivity());
        alertBuilder.setTitle("Please enable GPS to continue...");
        alertBuilder.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent settingsIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(settingsIntent);
                dialog.dismiss();
            }
        });
        alertBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertBuilder.create().show();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.Btn_LocateMe) {
            if (!appRuntime.isGPSEnabled()) {
                showGPSPromptWindow();
            } else if(appRuntime.heyCanIMakeNetworkCall()) {
                findNearByDonors();
            } else {
                Toast.makeText(mContext, "Please connect to network and configure accounts in settings.", Toast.LENGTH_SHORT).show();
            }

        }
    }

    private void findNearestDonorID() {
        new AsyncTask<Void, Void, String>() {
            String markerId = "";
            @Override
            protected void onPostExecute(String id) {
                if (id != null && !id.isEmpty() && donorCollection.containsKey(id)) {
                    try {
                        Marker marker = (Marker) donorCollection.get(id).get("marker");
                        marker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                        marker.showInfoWindow();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(mContext, "Not able to find nearest donor with 10 km", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            protected String doInBackground(Void... params) {
                try {
                    double nearestDonorDistance = 10000;
                    Location start = appRuntime.getLastKnowLocation();
                    if(start != null) {
                        Iterator iterator = donorCollection.entrySet().iterator();
                        while (iterator.hasNext()) {
                            Map.Entry pair = (Map.Entry) iterator.next();
                            JSONObject donor = (JSONObject) pair.getValue();
                            Location destination = new Location("");
                            destination.setLatitude(donor.getDouble(StaticStrings.LATITUDE));
                            destination.setLongitude(donor.getDouble(StaticStrings.LONGITUDE));
                            float distance = start.distanceTo(destination);
                            donor.put("distance", distance);
                            if (distance < nearestDonorDistance) {
                                nearestDonorDistance = distance;
                                markerId = (String) pair.getKey();
                            }
                        }
                        return markerId;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return markerId;
            }
        }.execute();
    }

    private class UpdatedDonorMarkerTask extends AsyncTask<Object, JSONObject, String> {
        private ProgressDialog dialog;
        private GoogleMap mapInstance;
        private Context mContext;
        private boolean isFirst;

        public UpdatedDonorMarkerTask(GoogleMap map, Context context) {
            mapInstance = map;
            mContext = context;
        }
        @Override
        protected void onPreExecute() {
            mMap.clear();
            donorCollection.clear();
            dialog = new ProgressDialog(getActivity());
            dialog.setTitle("Contacting to server...");
            dialog.setMessage("Fetching nearby donor from the server...");
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        @Override
        protected void onPostExecute(String result) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            if (result != null && result.equalsIgnoreCase("done")) {
                Toast.makeText(mContext, "Donors added to map", Toast.LENGTH_SHORT).show();
                findNearestDonorID();
                Location currentLocation = mapInstance.getMyLocation();
                if (currentLocation != null) {
                    LatLng position = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
                    MarkerOptions myOptions = new MarkerOptions()
                            .position(position)
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
                            .snippet("You are here")
                            .title(appRuntime.getDonorName());
                    Marker me = mapInstance.addMarker(myOptions);
                    mapInstance.animateCamera(CameraUpdateFactory.newLatLngZoom(position, 10));
                } else {
                    Toast.makeText(mContext, "Current location is unknown.", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(mContext, "Failed due to unknown reason", Toast.LENGTH_SHORT).show();
            }
        }

        private void drawDonorMarker(JSONObject donor) {
            try {
                double lat = donor.getDouble(StaticStrings.LATITUDE);
                double lon = donor.getDouble(StaticStrings.LONGITUDE);
                LatLng latLng = new LatLng(lat, lon);
                MarkerOptions markerOpt = new MarkerOptions();
                markerOpt.position(latLng);
                Marker donorMaker = mapInstance.addMarker(markerOpt);
                donor.put("marker", donorMaker);
                donorCollection.put(donorMaker.getId(), donor);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void onProgressUpdate(JSONObject... donor) {
            drawDonorMarker(donor[0]);
        }

        @Override
        protected String doInBackground(Object... params) {
            try {
                NetworkOperation findDonorsOperation = new NetworkOperation((String) params[0], (ContentValues) params[1], "GET", 5000, 5000);
                String response = findDonorsOperation.makeCall();
                if (response != null && !response.isEmpty()) {
                    JSONObject data = new JSONObject(response);
                    String msg = data.getString(StaticStrings.MSG);
                    if (msg != null && msg.equalsIgnoreCase("success")) {
                        JSONArray donor_list = data.getJSONArray("donors_list");
                        for (int i = 0; i < donor_list.length(); i++) {
                            publishProgress(donor_list.getJSONObject(i));
                        }
                        return "done";
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return null;
        }
    }

    private void findNearByDonors() {
        String locationAPI = appRuntime.getServerRESTAPIEndPoint() + "DonorListing";
        ContentValues params = new ContentValues();
        params.put("bloodGroup", (String) spinner_BloodGroup.getSelectedItem());
        params.put(StaticStrings.DONOR_MOBILE, appRuntime.getDonorMobile());
        params.put(StaticStrings.DONOR_CITY, "");
        params.put("geolocation", "true");
        UpdatedDonorMarkerTask updateTask = new UpdatedDonorMarkerTask(mMap, mContext);
        updateTask.execute(locationAPI, params);
    }

    class DonorMapInfoAdapter implements GoogleMap.InfoWindowAdapter {
        private View infoWindowView;

        DonorMapInfoAdapter() {
            infoWindowView = getActivity().getLayoutInflater().inflate(R.layout.donor_list_item, null);
            infoWindowView.setPadding(20, 20, 20, 20);
        }

        @Override
        public View getInfoWindow(Marker marker) {
            ImageView profile = (ImageView) infoWindowView.findViewById(R.id.profPic);
            TextView username = (TextView) infoWindowView.findViewById(R.id.donorName);
            TextView location = (TextView) infoWindowView.findViewById(R.id.location);
            profile.setImageResource(R.drawable.user_icon);
            String markerId = marker.getId();
            if (donorCollection.containsKey(markerId)) {
                try {
                    JSONObject donorInfo = donorCollection.get(markerId);
                    String name = donorInfo.getString(StaticStrings.DONOR_NAME);
                    String heartbeat = donorInfo.getString(StaticStrings.HEARTBEAT);
                    Date lastActivityOn = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(heartbeat);
                    String infoText = "Last seen at " + new SimpleDateFormat("MMM d, yyyy hh:mm a").format(lastActivityOn);

                    if (donorInfo.has("distance")) {
                        double distance = donorInfo.getDouble("distance");
                        if(distance > 999) {
                            distance = distance / 1000;
                            infoText += "\n\nDistance : " + String.format("%.2f", distance) + " km";
                        } else {
                            infoText += "\n\nDistance : " + String.format("%.2f", distance) + " m";
                        }

                    }
                    username.setText(name);
                    location.setText(infoText);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                //Toast.makeText(mContext, "Marker id Not found", Toast.LENGTH_SHORT).show();
                username.setText(appRuntime.getDonorName());
                location.setText(marker.getSnippet());
            }
            return infoWindowView;
        }

        @Override
        public View getInfoContents(Marker marker) {
            return null;
        }
    }

    class OnDonorMapClickListener implements GoogleMap.OnMapClickListener {
        @Override
        public void onMapClick(LatLng latLng) {
        }
    }

    class onDonorMarkerClickListener implements GoogleMap.OnMarkerClickListener {
        @Override
        public boolean onMarkerClick(Marker marker) {
            marker.showInfoWindow();
            return true;
        }
    }
    class onDonorInfoWindowClickListener implements GoogleMap.OnInfoWindowClickListener {
        @Override
        public void onInfoWindowClick(final Marker marker) {
            if (donorCollection.containsKey(marker.getId())) {


                JSONObject donor =  donorCollection.get(marker.getId());
                Bundle donor_info = new Bundle();
                Iterator<String> iterator = donor.keys();
                while(iterator.hasNext()) {
                    try {
                        String key = (String) iterator.next();
                        donor_info.putString(key, donor.getString(key));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                Intent profileIntent = new Intent(mContext, FloatingDonorProfile.class);
                profileIntent.putExtra("donor_info", donor_info);
                startActivity(profileIntent);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_maps_search, container, false);
        mContext = getActivity().getApplicationContext();
        appRuntime = LifeAppRuntime.getInstance(mContext);
        spinner_BloodGroup = (Spinner) rootView.findViewById(R.id.spinner_bloodgroup);
        ArrayAdapter spinnerAdapter = ArrayAdapter.createFromResource(mContext, R.array.blood_group_list, R.layout.spinner_view);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_BloodGroup.setAdapter(spinnerAdapter);
        donorCollection = new HashMap<>();
        setUpMapIfNeeded();
        mMap.setOnMapClickListener(new OnDonorMapClickListener());
        mMap.setOnMarkerClickListener(new onDonorMarkerClickListener());
        mMap.setInfoWindowAdapter(new DonorMapInfoAdapter());
        mMap.setOnInfoWindowClickListener(new onDonorInfoWindowClickListener());
        mBtn_LocateMe = (Button) rootView.findViewById(R.id.Btn_LocateMe);
        mBtn_LocateMe.setOnClickListener(this);
        return rootView;
    }

    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            SupportMapFragment mapFragment = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map));
            mMap = mapFragment.getMap();
            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            mMap.setMyLocationEnabled(true);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
