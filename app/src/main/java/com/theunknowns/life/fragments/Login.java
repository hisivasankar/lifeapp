package com.theunknowns.life.fragments;


import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.theunknowns.life.activity.Home;
import com.theunknowns.life.settings.LifeAppRuntime;
import com.theunknowns.life.util.Helper;
import com.theunknowns.life.util.NetworkOperation;
import com.theunknowns.life.util.StaticStrings;
import com.theunknows.life.life.R;

import org.json.JSONObject;

public class Login extends Fragment implements View.OnClickListener{

    private final static String LOG_TAG = Login.class.getSimpleName();
    private EditText editText_Mobile;
    private EditText editText_Password;
    private EditText editText_ServerIP;
    private Spinner spinner_Bloodgroup;
    private Button  btn_SignIn;
    private Button  btn_Register;

    private LifeAppRuntime appRuntime;
    private Context mContext;
    private Activity parent;

    public static Login newInstance(String param1, String param2) {
        Login fragment = new Login();
        return fragment;
    }

    public Login() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_login, container, false);
        mContext = getActivity().getApplicationContext();
        appRuntime = LifeAppRuntime.getInstance(mContext);
        parent = getActivity();

        onViewStateRestored(savedInstanceState);

        editText_Mobile = (EditText) rootView.findViewById(R.id.EditText_MobileNumber);
        editText_Password = (EditText) rootView.findViewById(R.id.EditText_Password);
        editText_ServerIP = (EditText) rootView.findViewById(R.id.EditText_ServerAddress);
        spinner_Bloodgroup = (Spinner) rootView.findViewById(R.id.Spinner_BloodGroup);
        btn_SignIn = (Button) rootView.findViewById(R.id.Btn_SignIn);
        btn_Register = (Button) rootView.findViewById(R.id.Btn_Register);

        editText_ServerIP.setText(appRuntime.getServerAddress());

        btn_SignIn.setOnClickListener(this);
        btn_Register.setOnClickListener(this);
        return rootView;
    }
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(StaticStrings.DONOR_MOBILE, editText_Mobile.getText().toString());
        outState.putString(StaticStrings.DONOR_PASSWORD, editText_Password.getText().toString());
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if(savedInstanceState != null) {
            editText_Mobile.setText(savedInstanceState.getString(StaticStrings.DONOR_NAME));
            editText_Password.setText(savedInstanceState.getString(StaticStrings.DONOR_PASSWORD));
        }
    }

    @Override
    public void onClick(View v) {
        int btnID = v.getId();
        if(btnID == R.id.Btn_SignIn) {
            if(appRuntime.isNetworkEnabled() && appRuntime.isGPSEnabled()) {
                doSignIn();
            } else {
                Toast.makeText(mContext, "Please enabled Network and GPS for Login/Signup", Toast.LENGTH_LONG).show();
            }
        } else if(btnID == R.id.Btn_Register) {
            doRegister();
        }
    }

    private void doSignIn() {
        String mobile = editText_Mobile.getText().toString();
        String password = editText_Password.getText().toString();
        String serverAddress = editText_ServerIP.getText().toString();
        String blood = spinner_Bloodgroup.getSelectedItem().toString();
        if(Helper.isValidParameters(mobile, password, serverAddress)) {
            appRuntime.setServerAddress(serverAddress);
            DoSignInTask task = new DoSignInTask(serverAddress, mobile, password, blood);
            task.execute();
        } else {
            Toast.makeText(mContext, "Please fill all required fields.", Toast.LENGTH_SHORT).show();
        }
    }
    private class DoSignInTask extends AsyncTask<Void, Void, String> {
        ProgressDialog dialog;
        String serverAddress, mobile, password, bloodgroup;
        public DoSignInTask(String serverIP, String mobileno, String passwd, String blood) {
            dialog = new ProgressDialog(parent);
            serverAddress = serverIP;
            mobile = mobileno;
            password = passwd;
            bloodgroup = blood;
        }

        @Override
        protected void onPreExecute() {
            dialog.setTitle("Signing in user ...");
            dialog.setMessage("Please Wait while we are connecting to server...");
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        @Override
        protected void onPostExecute(String result) {
            if(dialog.isShowing()) {
                dialog.dismiss();
            }
            if(result != null && result.equalsIgnoreCase("done")) {
                Toast.makeText(mContext, "Login Success...", Toast.LENGTH_SHORT).show();
                Intent homeIntent = new Intent(mContext, Home.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(homeIntent);
                parent.finish();
            } else {
                Toast.makeText(mContext, "Login failed.", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                String api = appRuntime.getServerRESTAPIEndPoint() + "Registration";
                ContentValues urlParams = new ContentValues();
                urlParams.put("islogin", true);
                urlParams.put(StaticStrings.DONOR_MOBILE,mobile);
                urlParams.put(StaticStrings.DONOR_PASSWORD, password);
                urlParams.put(StaticStrings.DONOR_BLOOD_GROUP, bloodgroup);
                NetworkOperation doSignInCall = new NetworkOperation(api, urlParams, "GET", 5000, 5000);
                String response = doSignInCall.makeCall();
                if(Helper.isValidParameters(response)) {
                    JSONObject data = new JSONObject(response);
                    if(data.getString(StaticStrings.MSG).equalsIgnoreCase(StaticStrings.SUCCESS)) {
                        appRuntime.setDonorName(data.getString(StaticStrings.DONOR_NAME));
                        appRuntime.setDonorMobile(data.getString(StaticStrings.DONOR_MOBILE));
                        appRuntime.setDonorEmail(data.getString(StaticStrings.DONOR_EMAIL));
                        appRuntime.setDonorPassword(data.getString(StaticStrings.DONOR_PASSWORD));
                        appRuntime.setDonorBloodGroup(data.getString(StaticStrings.DONOR_BLOOD_GROUP));
                        appRuntime.setDonorCity(data.getString(StaticStrings.DONOR_CITY));
                        return StaticStrings.DONE;
                    } else {
                        return StaticStrings.ERROR;
                    }
                }
            } catch (Exception ex) {
                Log.e(LOG_TAG, ex.getMessage());
                ex.printStackTrace();
            }
            return null;
        }
    }
    private void doRegister() {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Registration registration = new Registration();
        ft.replace(R.id.EntryContainer, registration);
        ft.addToBackStack(null);
        ft.commit();
    }
}
