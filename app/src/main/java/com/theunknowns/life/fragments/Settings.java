package com.theunknowns.life.fragments;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.theunknowns.life.activity.LoginRegistrationContainer;
import com.theunknowns.life.services.LifeLocationService;
import com.theunknowns.life.settings.LifeAppRuntime;
import com.theunknowns.life.util.Helper;
import com.theunknowns.life.util.NetworkOperation;
import com.theunknowns.life.util.StaticStrings;
import com.theunknows.life.life.R;

import org.json.JSONObject;

public class Settings extends Fragment implements View.OnClickListener {

    private final static String LOG_TAG = Settings.class.getSimpleName();

    private Context mContext;
    private LifeAppRuntime appRuntime;
    private Activity parent;

    private EditText editText_Username;
    private EditText editText_Mobile;
    private EditText editText_Password;
    private EditText editText_Email;
    private EditText editText_City;
    private EditText editText_ServerAddress;
    private Spinner spinner_Bloodgroup;

    private Button btn_Update, btn_Logout;
    public static Settings newInstance(String param1, String param2) {
        Settings fragment = new Settings();
        return fragment;
    }

    public Settings() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_settings, container, false);
        parent = getActivity();
        mContext = parent.getApplicationContext();
        appRuntime = LifeAppRuntime.getInstance(mContext);

        editText_Username = (EditText) rootView.findViewById(R.id.EditText_Username);
        editText_Mobile = (EditText) rootView.findViewById(R.id.EditText_MobileNumber);
        editText_Password = (EditText) rootView.findViewById(R.id.EditText_Password);
        editText_Email = (EditText) rootView.findViewById(R.id.EditText_Email);
        editText_City = (EditText) rootView.findViewById(R.id.EditText_Location);
        editText_ServerAddress = (EditText) rootView.findViewById(R.id.EditText_ServerAddress);
        spinner_Bloodgroup = (Spinner) rootView.findViewById(R.id.Spinner_BloodGroup);
        btn_Update = (Button) rootView.findViewById(R.id.Btn_Update);
        btn_Logout = (Button) rootView.findViewById(R.id.Btn_Logout);

        btn_Update.setOnClickListener(this);
        btn_Logout.setOnClickListener(this);

        updateDonorInformationInUI();

        return rootView;
    }
    private void updateDonorInformationInUI() {
        editText_Username.setText(appRuntime.getDonorName());
        editText_Mobile.setText(appRuntime.getDonorMobile());
        editText_Email.setText(appRuntime.getDonorEmail());
        editText_Password.setText(appRuntime.getDonorPassword());
        editText_City.setText(appRuntime.getDonorCity());
        editText_ServerAddress.setText(appRuntime.getServerAddress());
        spinner_Bloodgroup.setSelection(appRuntime.getDonorBloodGroupID());
    }
    @Override
    public void onClick(View v) {
        int viewID = v.getId();
        if(viewID == R.id.Btn_Update) {
            doUpdateServer();
        } else if(viewID == R.id.Btn_Logout) {
            appRuntime.destroySession();
            parent.stopService(new Intent(mContext, LifeLocationService.class));
            Intent goLoginRegistrationIntent = new Intent(mContext, LoginRegistrationContainer.class);
            goLoginRegistrationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            goLoginRegistrationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(goLoginRegistrationIntent);
            getActivity().finish();
        }
    }
    private void doUpdateServer() {
        String name = editText_Username.getText().toString();
        String email = editText_Email.getText().toString();
        String mobile = editText_Mobile.getText().toString();
        String password = editText_Password.getText().toString();
        String city = editText_City.getText().toString();
        String bloodgroup = spinner_Bloodgroup.getSelectedItem().toString();
        String serverAddress = editText_ServerAddress.getText().toString();
        if(Helper.isValidParameters(name, email, mobile, password, city, bloodgroup, serverAddress)) {
            appRuntime.setServerAddress(serverAddress);
            DoUpdateTask task = new DoUpdateTask(serverAddress, name, email, mobile, password, city, bloodgroup);
            task.execute();
        } else {
            Toast.makeText(mContext, "Please fill all required fields.", Toast.LENGTH_SHORT).show();
        }
    }
    private class DoUpdateTask extends AsyncTask<Void, Void, String> {
        ProgressDialog dialog;
        String serverAddress, username, email, mobile, password, city, bloodgroup;

        public DoUpdateTask(String server, String name, String mail, String phone, String passwd, String location, String blood) {
            dialog = new ProgressDialog(parent);
            serverAddress = server;
            username = name;
            email = mail;
            mobile = phone;
            password = passwd;
            city = location;
            bloodgroup = blood;
        }

        @Override
        protected void onPostExecute(String result) {
            if(dialog.isShowing()) {
                dialog.dismiss();
            }
            if(result != null && result.equalsIgnoreCase(StaticStrings.DONE)) {
                Toast.makeText(mContext, "Data updated successfully.", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(mContext, "Data update failed.", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected void onPreExecute() {
            dialog.setTitle("Creating User...");
            dialog.setMessage("Please Wait while we are connecting to server...");
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                String api = appRuntime.getServerRESTAPIEndPoint() + "Registration";
                ContentValues urlParams = new ContentValues();
                urlParams.put(StaticStrings.DONOR_NAME, username);
                urlParams.put(StaticStrings.DONOR_MOBILE,mobile);
                urlParams.put(StaticStrings.DONOR_EMAIL, email);
                urlParams.put(StaticStrings.DONOR_PASSWORD, password);
                urlParams.put(StaticStrings.DONOR_BLOOD_GROUP, bloodgroup);
                urlParams.put(StaticStrings.DONOR_CITY, city);
                NetworkOperation doUpdate = new NetworkOperation(api, urlParams, "GET", 5000, 5000);
                String response = doUpdate.makeCall();
                if(Helper.isValidParameters(response)) {
                    JSONObject data = new JSONObject(response);
                    if(data.getString(StaticStrings.MSG).equalsIgnoreCase(StaticStrings.SUCCESS)) {
                        appRuntime.setDonorName(data.getString(StaticStrings.DONOR_NAME));
                        appRuntime.setDonorMobile(data.getString(StaticStrings.DONOR_MOBILE));
                        appRuntime.setDonorEmail(data.getString(StaticStrings.DONOR_EMAIL));
                        appRuntime.setDonorPassword(data.getString(StaticStrings.DONOR_PASSWORD));
                        appRuntime.setDonorBloodGroup(data.getString(StaticStrings.DONOR_BLOOD_GROUP));
                        appRuntime.setDonorCity(data.getString(StaticStrings.DONOR_CITY));
                        return StaticStrings.DONE;
                    } else {
                        return StaticStrings.ERROR;
                    }
                }
            } catch (Exception ex) {
                Log.e(LOG_TAG, ex.getMessage());
                ex.printStackTrace();
            }
            return null;
        }
    }
}
