package com.theunknowns.life.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.theunknowns.life.activity.DonorListing;
import com.theunknowns.life.settings.LifeAppRuntime;
import com.theunknowns.life.util.NetworkOperation;
import com.theunknowns.life.util.StaticStrings;
import com.theunknows.life.life.R;

import org.json.JSONArray;
import org.json.JSONObject;


public class BloodGroupSearch extends Fragment implements View.OnClickListener{

    private static final String LOG_TAG = BloodGroupSearch.class.getSimpleName();
    private LifeAppRuntime appRuntime;
    private Context context;

    public static BloodGroupSearch newInstance(String param1, String param2) {
        BloodGroupSearch fragment = new BloodGroupSearch();
        return fragment;
    }

    public BloodGroupSearch() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if(id == R.id.Btn_Search) {
            doTraditionalSearch();
        } else if(id == R.id.Btn_Broadcast) {
            sendBroadCastRequest();

        }
    }
    private void doTraditionalSearch() {
        Spinner spinner = (Spinner) getActivity().findViewById(R.id.blood_group_spiner);
        String location = ((EditText)getActivity().findViewById(R.id.EditText_Location)).getText().toString();
        String requiredBlood = (String) spinner.getSelectedItem();
        if (appRuntime.heyCanIMakeNetworkCall()) {
            String requestAPI = appRuntime.getServerRESTAPIEndPoint() + "DonorListing";
            ContentValues params = new ContentValues();
            params.put("bloodGroup", requiredBlood);
            params.put(StaticStrings.DONOR_MOBILE, appRuntime.getDonorMobile());
            params.put(StaticStrings.DONOR_CITY, location);
            new FindDonorListingTask().execute(requestAPI, params);
        } else {
            Toast.makeText(context, "Please connect to network or configure accounts in settings.", Toast.LENGTH_SHORT).show();
        }
    }
    private void sendBroadCastRequest() {
        Spinner spinner = (Spinner) getActivity().findViewById(R.id.blood_group_spiner);
        String location = ((EditText)getActivity().findViewById(R.id.EditText_Location)).getText().toString();
        String requiredBlood = (String) spinner.getSelectedItem();
        if (appRuntime.heyCanIMakeNetworkCall()) {
            final String requestAPI = appRuntime.getServerRESTAPIEndPoint() + "SendPushRequest";
            ContentValues urlParams = new ContentValues();
            urlParams.put(StaticStrings.DONOR_MOBILE, appRuntime.getDonorMobile());
            urlParams.put(StaticStrings.ISBRODADCAST, true);
            urlParams.put(StaticStrings.BROADCAST_BLOODGROUP, requiredBlood);
            //params.put(StaticStrings.DONOR_CITY, location);
            new AsyncTask<Object, Void, String>() {
                private ProgressDialog dialog;
                private int succesCount;
                private String msg_desc;
                @Override
                protected void onPreExecute() {
                    dialog = new ProgressDialog(getActivity());
                    dialog.setCancelable(false);
                    dialog.setCanceledOnTouchOutside(false);
                    dialog.setTitle("Sending Broadcast");
                    dialog.setMessage("Contacting server to send broadcast request.");
                    dialog.show();
                }
                @Override
                protected void onPostExecute(String s) {
                    if(dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    if(s != null && s.equals(StaticStrings.DONE)) {
                        Toast.makeText(getActivity().getApplicationContext(), "Broadcast sent to " + succesCount + " donors", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getActivity().getApplicationContext(), msg_desc, Toast.LENGTH_LONG).show();
                    }
                }
                @Override
                protected String doInBackground(Object... params) {
                    try {
                        NetworkOperation task = new NetworkOperation(requestAPI, (ContentValues) params[0], "GET", 5000, 5000);
                        String resp = task.makeCall();
                        JSONObject data = new JSONObject(resp);
                        if(data.getString(StaticStrings.MSG).equalsIgnoreCase(StaticStrings.SUCCESS)) {
                            succesCount = data.getInt(StaticStrings.SUCCESS);
                            msg_desc = data.getString(StaticStrings.MSG_DESC);
                            return StaticStrings.DONE;
                        } else {
                            msg_desc = data.getString(StaticStrings.MSG_DESC);
                            return null;
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        Log.e(LOG_TAG, ex.getMessage());
                    }
                    return null;
                }
            }.execute(urlParams);
        } else {
            Toast.makeText(context, "Please connect to network or configure accounts in settings.", Toast.LENGTH_SHORT).show();
        }
    }

    private class FindDonorListingTask extends AsyncTask<Object, Void, JSONArray> {
        ProgressDialog dialog;
        public FindDonorListingTask() {
            dialog = new ProgressDialog(getActivity());
        }
        @Override
        protected JSONArray doInBackground(Object... params) {
            try {
                NetworkOperation donorListingOperation = new NetworkOperation((String) params[0], (ContentValues)params[1], "GET", 10000, 10000);
                String resp = donorListingOperation.makeCall();
                if(resp != null && !resp.isEmpty()) {
                    JSONObject data = new JSONObject(resp);
                    JSONArray donors = data.getJSONArray("donors_list");
                    return donors;
                }
            } catch (Exception ex) {
                Log.e(LOG_TAG, "Error : " + ex.getMessage());
            }
            return null;
        }
        @Override
        protected void onPostExecute(JSONArray result) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            if(result == null) {
                Toast.makeText(getActivity().getApplicationContext(), "Failed due to known reason.", Toast.LENGTH_LONG).show();
            } else if(result.length() == 0) {
                Toast.makeText(getActivity().getApplicationContext(), "No donors found.", Toast.LENGTH_LONG).show();
            } else {
                Intent listing = new Intent(getActivity().getApplicationContext(), DonorListing.class);
                listing.putExtra("donors_list", result.toString());
                startActivity(listing);
            }
        }
        @Override
        protected void onPreExecute() {
            dialog.setTitle("Connecting...");
            dialog.setMessage("Contacting server to fetch donors.");
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View rootView = inflater.inflate(R.layout.fragment_blood_group_search, container, false);
        context = getActivity().getApplicationContext();
        appRuntime = LifeAppRuntime.getInstance(context);

        Spinner spinner_blood_type = (Spinner) rootView.findViewById(R.id.blood_group_spiner);
        ArrayAdapter<CharSequence> spinner_Adapter = ArrayAdapter.createFromResource(
                getActivity().getApplicationContext(),
                R.array.blood_group_list,
                R.layout.spinner_view);
        spinner_Adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner_blood_type.setAdapter(spinner_Adapter);
        Button Btn_Search = (Button) rootView.findViewById(R.id.Btn_Search);
        Btn_Search.setOnClickListener(this);
        Button Btn_Broadcast = (Button) rootView.findViewById(R.id.Btn_Broadcast);
        Btn_Broadcast.setOnClickListener(this);


        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
