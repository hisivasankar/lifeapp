package com.theunknowns.life.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.theunknowns.life.settings.LifeAppRuntime;
import com.theunknowns.life.util.StaticStrings;
import com.theunknows.life.life.R;

import org.json.JSONObject;

import java.util.List;
import java.util.Locale;

public class DonorProfile extends Fragment implements View.OnClickListener {

    private TextView mUsername;
    private TextView mMobile;
    private TextView mEmail;
    private TextView mBloodGroup;
    private TextView mLocation;
    LinearLayout row_Mobile, row_Email;
    private Context mContext;

    public static final String LOG_TAG = DonorProfile.class.getSimpleName();

    public static DonorProfile newInstance(String param1, String param2) {
        DonorProfile fragment = new DonorProfile();
        return fragment;
    }

    public DonorProfile() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private void setProfile(String name, String mobile, String email, String blood, String location) {
        mUsername.setText(name);
        mMobile.setText(mobile);
        mEmail.setText(email);
        mBloodGroup.setText(blood);
        mLocation.setText(location);
    }
    private class GetAddressFromGeoCoder extends AsyncTask<Void, Void, String> {
        private JSONObject donor;
        private double lat, lon;
        public GetAddressFromGeoCoder(JSONObject donor_info) {
            donor = donor_info;
        }
        public GetAddressFromGeoCoder(String lat, String lon) {
            this.lat = Double.parseDouble(lat);
            this.lon = Double.parseDouble(lon);
        }

        @Override
        protected void onPostExecute(String result) {
            if(result != null) {
                mLocation.setText(result);
            }
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                double lat, lon;
                if(donor != null) {
                    lat = donor.getDouble(StaticStrings.LATITUDE);
                    lon = donor.getDouble(StaticStrings.LONGITUDE);
                } else {
                    lat = this.lat;
                    lon = this.lon;
                }
                Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());
                List<Address> address = geocoder.getFromLocation(lat, lon, 1);
                if(address != null) {
                    Address currentLocation = address.get(0);
                    StringBuilder myLocation = new StringBuilder();
                    for(int i=0;i<currentLocation.getMaxAddressLineIndex(); i++) {
                        myLocation.append(currentLocation.getAddressLine(i) + ",\n");
                    }
                    myLocation.append(currentLocation.getCountryName());
                    Log.i(LOG_TAG, myLocation.toString());
                    return myLocation.toString();
                } else {
                    Log.i(LOG_TAG, "No Address found");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }
    private void parseUserDataFromJSON(String data) {
        try {
            JSONObject user = new JSONObject(data);
            if(user.has(StaticStrings.LATITUDE) && user.has(StaticStrings.LONGITUDE)) {
                setProfile(user.getString(StaticStrings.DONOR_NAME), user.getString(StaticStrings.DONOR_MOBILE),
                        user.getString(StaticStrings.DONOR_EMAIL), user.getString(StaticStrings.DONOR_BLOOD_GROUP), user.getString(StaticStrings.DONOR_CITY));
                GetAddressFromGeoCoder task = new GetAddressFromGeoCoder(user);
                task.execute();
            } else {
                setProfile(user.getString(StaticStrings.DONOR_NAME), user.getString(StaticStrings.DONOR_MOBILE),
                        user.getString(StaticStrings.DONOR_EMAIL), user.getString(StaticStrings.DONOR_BLOOD_GROUP), user.getString(StaticStrings.DONOR_CITY));
            }
            row_Email.setOnClickListener(this);
            row_Mobile.setOnClickListener(this);
        } catch (Exception ex) {
            ex.printStackTrace();
            Log.e(LOG_TAG, "Error: " + ex.getMessage());
        }
    }

    private void updateFromBundle(Bundle donor_info) {
        String name = donor_info.getString(StaticStrings.DONOR_NAME);
        String mobile = donor_info.getString(StaticStrings.DONOR_MOBILE);
        String email = donor_info.getString(StaticStrings.DONOR_EMAIL);
        String blood = donor_info.getString(StaticStrings.DONOR_BLOOD_GROUP);
        String city = donor_info.getString(StaticStrings.DONOR_CITY);
        if(donor_info.containsKey(StaticStrings.REQUESTED_BLOOD_GROUP) == true) {
            blood = "Required Blood Group : " + donor_info.getString(StaticStrings.REQUESTED_BLOOD_GROUP);
            setProfile(name, mobile, email, blood, city);
        } else {
            setProfile(name, mobile, email, blood, city);
        }

        if(donor_info.containsKey(StaticStrings.LATITUDE) && donor_info.containsKey(StaticStrings.LONGITUDE)) {
            GetAddressFromGeoCoder task = new GetAddressFromGeoCoder(donor_info.getString(StaticStrings.LATITUDE), donor_info.getString(StaticStrings.LONGITUDE));
            task.execute();
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_donor_profile, container, false);
        mContext = getActivity().getApplicationContext();

        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.appbar);
        if(toolbar != null) {
            toolbar.setVisibility(View.GONE);
        }
        mUsername = (TextView) rootView.findViewById(R.id.textView_Username);
        mMobile = (TextView) rootView.findViewById(R.id.textView_Mobile);
        mEmail = (TextView) rootView.findViewById(R.id.textView_Email);
        mBloodGroup = (TextView) rootView.findViewById(R.id.textView_BloodGroup);
        mLocation = (TextView) rootView.findViewById(R.id.textView_Location);
        row_Mobile = (LinearLayout) rootView.findViewById(R.id.Row_Mobile);
        row_Email = (LinearLayout) rootView.findViewById(R.id.Row_Email);
        Bundle donor_info = getArguments();
        if(donor_info != null) {
            updateFromBundle(donor_info);
        } else {
            donor_info = getActivity().getIntent().getBundleExtra(StaticStrings.BROADCAST_REQUEST);
            if(donor_info != null) {
                updateFromBundle(donor_info);
            } else if(getActivity().getIntent().getBundleExtra("donor_info") != null){
                updateFromBundle(getActivity().getIntent().getBundleExtra("donor_info"));
            } else {
                Toast.makeText(mContext, "User Data is empty", Toast.LENGTH_SHORT).show();
            }
        }
        return rootView;
    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View v) {
        int viewObjectID = v.getId();
        if (viewObjectID == R.id.Row_Mobile) {
            String mobileInfo = "tel:" + mMobile.getText().toString();
            Log.i(LOG_TAG, "Mobile = " + mobileInfo);
            Intent dialerIntent = new Intent(Intent.ACTION_DIAL, Uri.parse(mobileInfo));
            startActivity(dialerIntent);
        } else if (viewObjectID == R.id.Row_Email) {
            String[] recipient = new String[]{mEmail.getText().toString()};
            Log.i(LOG_TAG, "Email =  " + recipient.toString());
            String msg = "Hi buddy, I am " + LifeAppRuntime.getInstance(mContext).getDonorName() + ". There is a urgent requirement for blood group of your type. Please write back";
            Intent emailIntent = new Intent(Intent.ACTION_SEND);
            emailIntent.setType("text/plain");
            emailIntent.putExtra(Intent.EXTRA_EMAIL, recipient);
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Request to donate blood");
            emailIntent.putExtra(Intent.EXTRA_TEXT, msg);
            startActivity(Intent.createChooser(emailIntent, "Sending Email..."));
        }
    }
}
