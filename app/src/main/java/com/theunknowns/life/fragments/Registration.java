package com.theunknowns.life.fragments;


import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.theunknowns.life.activity.Home;
import com.theunknowns.life.settings.LifeAppRuntime;
import com.theunknowns.life.util.Helper;
import com.theunknowns.life.util.NetworkOperation;
import com.theunknowns.life.util.StaticStrings;
import com.theunknows.life.life.R;

import org.json.JSONObject;

public class Registration extends Fragment implements View.OnClickListener{
    private final static String LOG_TAG = Registration.class.getSimpleName();
    private LifeAppRuntime appRuntime;
    private Activity parent;
    private Context mContext;

    private EditText editText_Username;
    private EditText editText_Mobile;
    private EditText editText_Password;
    private EditText editText_Email;
    private EditText editText_City;
    private EditText editText_ServerAddress;
    private Spinner spinner_Bloodgroup;

    private Button btn_Register;

    public static Registration newInstance(String param1, String param2) {
        Registration fragment = new Registration();
        return fragment;
    }

    public Registration() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_registration, container, false);

        appRuntime = LifeAppRuntime.getInstance(getActivity().getApplicationContext());
        parent = getActivity();
        mContext = parent.getApplicationContext();

        onViewStateRestored(savedInstanceState);

        editText_Username = (EditText) rootView.findViewById(R.id.EditText_Username);
        editText_Mobile = (EditText) rootView.findViewById(R.id.EditText_MobileNumber);
        editText_Password = (EditText) rootView.findViewById(R.id.EditText_Password);
        editText_Email = (EditText) rootView.findViewById(R.id.EditText_Email);
        editText_City = (EditText) rootView.findViewById(R.id.EditText_Location);
        editText_ServerAddress = (EditText) rootView.findViewById(R.id.EditText_ServerAddress);
        spinner_Bloodgroup = (Spinner) rootView.findViewById(R.id.Spinner_BloodGroup);

        btn_Register = (Button) rootView.findViewById(R.id.Btn_SignIn);
        btn_Register.setOnClickListener(this);

        editText_ServerAddress.setText(appRuntime.getServerAddress());

        return rootView;
    }
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(StaticStrings.DONOR_NAME, editText_Username.getText().toString());
        outState.putString(StaticStrings.DONOR_MOBILE, editText_Mobile.getText().toString());
        outState.putString(StaticStrings.DONOR_PASSWORD, editText_Password.getText().toString());
        outState.putString(StaticStrings.DONOR_EMAIL, editText_Email.getText().toString());
        outState.putString(StaticStrings.DONOR_CITY, editText_City.getText().toString());
        outState.putInt(StaticStrings.DONOR_BLOOD_GROUP_ID, spinner_Bloodgroup.getSelectedItemPosition());
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if(savedInstanceState != null) {
            editText_Username.setText(savedInstanceState.getString(StaticStrings.DONOR_NAME));
            editText_Mobile.setText(savedInstanceState.getString(StaticStrings.DONOR_NAME));
            editText_Password.setText(savedInstanceState.getString(StaticStrings.DONOR_PASSWORD));
            editText_Email.setText(savedInstanceState.getString(StaticStrings.DONOR_EMAIL));
            editText_City.setText(savedInstanceState.getString(StaticStrings.DONOR_CITY));
            spinner_Bloodgroup.setSelection(savedInstanceState.getInt(StaticStrings.DONOR_BLOOD_GROUP_ID));
        }
    }

    @Override
    public void onClick(View v) {
        int viewID = v.getId();
        if(viewID == R.id.Btn_SignIn) {
            if(appRuntime.isNetworkEnabled() && appRuntime.isGPSEnabled()) {
                doRegistration();
            } else {
                Toast.makeText(mContext, "Please enabled Network and GPS for Login/Signup", Toast.LENGTH_LONG).show();
            }

        }
    }
    private void doRegistration() {
        String name = editText_Username.getText().toString();
        String email = editText_Email.getText().toString();
        String mobile = editText_Mobile.getText().toString();
        String password = editText_Password.getText().toString();
        String city = editText_City.getText().toString();
        String bloodgroup = spinner_Bloodgroup.getSelectedItem().toString();
        String serverAddress = editText_ServerAddress.getText().toString();
        if(Helper.isValidParameters(name, email, mobile, password, city, bloodgroup, serverAddress)) {
            appRuntime.setServerAddress(serverAddress);
            DoRegistrationTask task = new DoRegistrationTask(serverAddress, name, email, mobile, password, city, bloodgroup);
            task.execute();
        } else {
            Toast.makeText(mContext, "Please fill all required fields.", Toast.LENGTH_SHORT).show();
        }
    }
    private class DoRegistrationTask extends AsyncTask<Void, Void, String> {
        ProgressDialog dialog;
        String serverAddress, username, email, mobile, password, city, bloodgroup;

        public DoRegistrationTask(String server, String name, String mail, String phone, String passwd, String location, String blood) {
            dialog = new ProgressDialog(parent);
            serverAddress = server;
            username = name;
            email = mail;
            mobile = phone;
            password = passwd;
            city = location;
            bloodgroup = blood;
        }

        @Override
        protected void onPostExecute(String result) {
            if(dialog.isShowing()) {
                dialog.dismiss();
            }
            if(result != null && result.equalsIgnoreCase(StaticStrings.DONE)) {
                Toast.makeText(mContext, "User Registration Successful.", Toast.LENGTH_SHORT).show();
                Intent homeIntent = new Intent(mContext, Home.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(homeIntent);
                parent.finish();
            } else {
                Toast.makeText(mContext, "User Registration failed.", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected void onPreExecute() {
            dialog.setTitle("Creating User...");
            dialog.setMessage("Please Wait while we are connecting to server...");
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                String api = appRuntime.getServerRESTAPIEndPoint() + "Registration";
                ContentValues urlParams = new ContentValues();
                urlParams.put(StaticStrings.DONOR_NAME, username);
                urlParams.put(StaticStrings.DONOR_MOBILE,mobile);
                urlParams.put(StaticStrings.DONOR_EMAIL, email);
                urlParams.put(StaticStrings.DONOR_PASSWORD, password);
                urlParams.put(StaticStrings.DONOR_BLOOD_GROUP, bloodgroup);
                urlParams.put(StaticStrings.DONOR_CITY, city);
                NetworkOperation doSignInCall = new NetworkOperation(api, urlParams, "GET", 5000, 5000);
                String response = doSignInCall.makeCall();
                if(Helper.isValidParameters(response)) {
                    JSONObject data = new JSONObject(response);
                    if(data.getString(StaticStrings.MSG).equalsIgnoreCase(StaticStrings.SUCCESS)) {
                        appRuntime.setDonorName(data.getString(StaticStrings.DONOR_NAME));
                        appRuntime.setDonorMobile(data.getString(StaticStrings.DONOR_MOBILE));
                        appRuntime.setDonorEmail(data.getString(StaticStrings.DONOR_EMAIL));
                        appRuntime.setDonorPassword(data.getString(StaticStrings.DONOR_PASSWORD));
                        appRuntime.setDonorBloodGroup(data.getString(StaticStrings.DONOR_BLOOD_GROUP));
                        appRuntime.setDonorCity(data.getString(StaticStrings.DONOR_CITY));
                        return StaticStrings.DONE;
                    } else {
                        return StaticStrings.ERROR;
                    }
                }
            } catch (Exception ex) {
                Log.e(LOG_TAG, ex.getMessage());
                ex.printStackTrace();
            }
            return null;
        }
    }
}
