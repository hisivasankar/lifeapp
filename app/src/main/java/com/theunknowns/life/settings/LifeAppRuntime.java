package com.theunknowns.life.settings;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.theunknowns.life.util.Helper;
import com.theunknowns.life.util.NetworkOperation;
import com.theunknowns.life.util.StaticStrings;
import com.theunknows.life.life.R;

import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by I308944 on 5/26/2015.
 */
public class LifeAppRuntime {
    private final static String LOG_TAG = LifeAppRuntime.class.getSimpleName();
    private static LifeAppRuntime appSettings;

    private Context mContext;

    private String serverProtocol;
    private String serverAddress;
    private String serverPort;
    private String serverAPIEndPoint;

    private Location myLocation;

    private String donor_mobile;
    private String donor_name;
    private String donor_city;
    private String donor_password;
    private String donor_email;
    private String donor_bloodgroup;
    private String donor_gcm_token;
    private int appVersion;


    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    private LifeAppRuntime(Context context) {
        mContext = context;
        serverProtocol = "http://";
        serverAPIEndPoint = "/LifeServer/API/";
        serverPort = "8080";
        preferences = context.getSharedPreferences(context.getString(R.string.preference_filename), Context.MODE_PRIVATE);
        editor = preferences.edit();

        String serverIP = preferences.getString(context.getString(R.string.preference_key_serverIP), "");

        String mobile = preferences.getString(StaticStrings.DONOR_MOBILE, "");
        String name = preferences.getString(StaticStrings.DONOR_NAME, "");
        String city = preferences.getString(StaticStrings.DONOR_CITY, "");
        String password = preferences.getString(StaticStrings.DONOR_PASSWORD, "");
        String email = preferences.getString(StaticStrings.DONOR_EMAIL, "");
        String bloodgroup = preferences.getString(StaticStrings.DONOR_BLOOD_GROUP, "");

        String token = preferences.getString(StaticStrings.DONOR_GCM_TOKEN, "");
        int version = preferences.getInt(StaticStrings.APPVERSION, Integer.MIN_VALUE);

        this.serverAddress = serverIP;
        this.donor_mobile = mobile;
        this.donor_name = name;
        this.donor_city = city;
        this.donor_password = password;
        this.donor_email = email;
        this.donor_bloodgroup = bloodgroup;

        this.donor_gcm_token = token;
        this.appVersion = version;
    }
    public static LifeAppRuntime getInstance(Context appContext) {
        if(appSettings != null) {
            return appSettings;
        } else {
            appSettings = new LifeAppRuntime(appContext);
            return  appSettings;
        }
    }
    public void setLastKnowLocation(Location location) {
        this.myLocation = location;
    }
    public Location getLastKnowLocation() {
        return this.myLocation;
    }
    public boolean isNetworkEnabled() {
        ConnectivityManager connectivityManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = connectivityManager.getActiveNetworkInfo();
        if(info != null && info.isConnected()) {
            return true;
        } else {
            return false;
        }
    }
    public boolean isGPSEnabled() {
        LocationManager locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        if(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            return true;
        } else {
            return false;
        }
    }

    public String getServerAddress() {
        return serverAddress;
    }
    public String getServerRESTAPIEndPoint() {
        return this.serverProtocol + this.serverAddress + ":" + this.serverPort + this.serverAPIEndPoint;
    }
    public void setServerAddress(String server) {
        editor.putString(mContext.getString(R.string.preference_key_serverIP), server);
        editor.commit();
        this.serverAddress = server;
    }
    public boolean heyCanIMakeNetworkCall() {
        if(Helper.isValidParameters(this.serverAddress, this.donor_mobile) && isNetworkEnabled() == true) {
            return true;
        } else {
            return false;
        }
    }
    public boolean isAccountsConfigured() {
        if(Helper.isValidParameters(this.serverAddress, this.donor_mobile, this.donor_name)){
            return true;
        } else {
            return false;
        }
    }
    public void destroySession() {
        setDonorMobile("");
        setDonorName("");
        setDonorCity("");
        setDonorBloodGroup("");
        setDonorEmail("");
        setDonorPassword("");
        setOnlyGCMToken("");
    }

    // Default User data getter and setters

    public void setDonorName(String name) {
        this.donor_name = name;
        editor.putString(StaticStrings.DONOR_NAME, this.donor_name);
        editor.commit();

    }
    public void setDonorMobile(String mobile) {
        this.donor_mobile = mobile;
        editor.putString(StaticStrings.DONOR_MOBILE, this.donor_mobile);
        editor.commit();

    }
    public void setDonorEmail(String email) {
        this.donor_email = email;
        editor.putString(StaticStrings.DONOR_EMAIL, this.donor_email);
        editor.commit();
    }
    public void setDonorCity(String city) {
        this.donor_city = city;
        editor.putString(StaticStrings.DONOR_CITY, this.donor_city);
        editor.commit();
    }
    public void setDonorPassword(String password) {
        this.donor_password = password;
        editor.putString(StaticStrings.DONOR_PASSWORD, this.donor_password);
        editor.commit();
    }
    public void setDonorBloodGroup(String blood) {
        this.donor_bloodgroup = blood;
        editor.putString(StaticStrings.DONOR_BLOOD_GROUP, this.donor_bloodgroup);
        editor.commit();
    }

    public String getDonorName() {
        return this.donor_name;
    }
    public String getDonorMobile(){
        return this.donor_mobile;
    }

    public String getDonorEmail(){
        return this.donor_email;
    }

    public String getDonorPassword(){
        return this.donor_password;
    }

    public String getDonorCity(){
        return this.donor_city;
    }

    public String getDonorBloodGroup(){
        return this.donor_bloodgroup;
    }
    public int getDonorBloodGroupID() {
        return Helper.getBloodGroupId(this.donor_bloodgroup);
    }

    public String getGCMToken() {
        String token = this.donor_gcm_token;
        if(token.isEmpty()) {
            return "";
        }
        int currentVersion = getAppVersion();
        if(currentVersion != this.appVersion) {
            return "";
        }
        return this.donor_gcm_token;
    }
    public int getAppVersion() {
        try {
            PackageInfo info = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0);
            return info.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("can't get package name");
        }
    }
    public int getStoreAppVersion() {
        return this.appVersion;
    }
    private void setGCMToken(String token) {
        this.donor_gcm_token = token;
        editor.putString(StaticStrings.DONOR_GCM_TOKEN, this.donor_gcm_token);
        this.appVersion = getAppVersion();
        editor.putInt(StaticStrings.APPVERSION, this.appVersion);
        editor.commit();
    }
    private void setOnlyGCMToken(String token) {
        this.donor_gcm_token = token;
        editor.putString(StaticStrings.DONOR_GCM_TOKEN, this.donor_gcm_token);
        editor.commit();
    }
    private void subscribeToGCM() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected void onPostExecute(String result) {
                if (result != null) {
                    setGCMToken(result);
                    Log.i(LOG_TAG, "GCM Token : " + result);
                    Toast.makeText(mContext, "Subscribed to push notification", Toast.LENGTH_SHORT).show();
                } else {
                    Log.i(LOG_TAG, "Failed to registered GCM");
                    Toast.makeText(mContext, "Failed to connect to push notification server", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            protected String doInBackground(Void... params) {
                try {
                    InstanceID instanceID = InstanceID.getInstance(mContext);
                    String token = instanceID.getToken(StaticStrings.SENDER_ID, GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                    Log.i(LOG_TAG, "Simple Token : " + token);
                    return token;
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute();
    }
    public void initializePushNotification() {
        if(getGCMToken().isEmpty()) {
            subscribeToGCM();
        }
        updateGCMTokenInAPPServer();
    }
    public void updateGCMTokenInAPPServer() {
        if(!getGCMToken().isEmpty()) {
            Log.i(LOG_TAG, "GCM Token to Server: " + getGCMToken());
            new AsyncTask<Void, Void, String>() {
                @Override
                protected void onPostExecute(String result) {
                    if(result != null && result.equalsIgnoreCase(StaticStrings.DONE)) {
                        Log.i(LOG_TAG, "GCM Token Updated in server");
                    } else {
                        Log.i(LOG_TAG, "Failed to update GCM Token in server");
                    }
                }
                @Override
                protected String doInBackground(Void... params) {
                    try {
                        String api = getServerRESTAPIEndPoint() + "RegisterGCMToken";
                        ContentValues qryParams = new ContentValues();
                        qryParams.put(StaticStrings.DONOR_MOBILE, getDonorMobile());
                        qryParams.put(StaticStrings.DONOR_GCM_TOKEN, getGCMToken());
                        NetworkOperation task = new NetworkOperation(api, qryParams, "GET", 5000, 5000);
                        String resp = task.makeCall();
                        JSONObject data = new JSONObject(resp);
                        if(data.getString(StaticStrings.MSG).equalsIgnoreCase(StaticStrings.SUCCESS)) {
                            return StaticStrings.DONE;
                        } else {
                            return StaticStrings.ERROR;
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    return null;
                }
            }.execute();
        } else {
            subscribeToGCM();
        }
    }

}
