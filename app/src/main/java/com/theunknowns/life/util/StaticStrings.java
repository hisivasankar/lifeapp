package com.theunknowns.life.util;

/**
 * Created by I308944 on 6/7/2015.
 */
public class StaticStrings {
    public final static String DONOR_NAME = "donor_name";
    public final static String DONOR_MOBILE = "donor_mobile";
    public final static String DONOR_PASSWORD = "donor_password";
    public final static String DONOR_EMAIL = "donor_email";
    public final static String DONOR_CITY = "donor_city";
    public final static String DONOR_BLOOD_GROUP = "donor_blood_group";
    public final static String DONOR_BLOOD_GROUP_ID = "donor_blood_group_id";
    public final static String ISBRODADCAST = "is_broadcast";
    public final static String BROADCAST_BLOODGROUP = "broadcast_bloodgroup";

    public final static String LATITUDE = "latitude";
    public final static String LONGITUDE = "longitude";
    public final static String HEARTBEAT = "heartbeat";
    public final static String MSG = "msg";
    public final static String MSG_DESC = "msg_desc";
    public final static String SUCCESS = "success";
    public final static String ERROR = "error";
    public final static String DONE = "done";
    public final static String REQUESTED_BLOOD_GROUP = "requested_blood_group";
    public final static String BROADCAST_REQUEST = "BroadCastRequest";
    public final static String DONOR_GCM_TOKEN = "donor_gcm_token";
    public final static String APPVERSION = "app_version";

    public final static String SENDER_ID = "1030477556136";

}
