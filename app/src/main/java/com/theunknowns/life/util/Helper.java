package com.theunknowns.life.util;

import java.util.HashMap;

/**
 * Created by I308944 on 6/9/2015.
 */
public class Helper {
    public static boolean isValidParameters(String... params) {
        for (int i = 0; i < params.length; i++) {
            if (params[i] == null || params[i].trim().isEmpty()) {
                return false;
            }
        }
        return true;
    }

    public static int getBloodGroupId(String bloodgroup) {
        HashMap<String, Integer> bloodMap = new HashMap<String, Integer>();
        bloodMap.put("A+ve".toLowerCase(), 0);
        bloodMap.put("B+ve".toLowerCase(), 1);
        bloodMap.put("O+ve".toLowerCase(), 2);
        bloodMap.put("AB+ve".toLowerCase(), 3);

        bloodMap.put("A-ve".toLowerCase(), 4);
        bloodMap.put("B-ve".toLowerCase(), 5);
        bloodMap.put("O-ve".toLowerCase(), 6);
        bloodMap.put("AB-ve".toLowerCase(), 7);

        bloodMap.put("A1+ve".toLowerCase(), 8);
        bloodMap.put("A1-ve".toLowerCase(), 9);

        bloodMap.put("A1B+ve".toLowerCase(), 10);
        bloodMap.put("A1B-ve".toLowerCase(), 11);
        if (bloodgroup != null && !bloodgroup.isEmpty() && bloodMap.containsKey(bloodgroup.toLowerCase())) {
            return bloodMap.get(bloodgroup.toLowerCase());
        } else {
            return 0;
        }
    }
}
