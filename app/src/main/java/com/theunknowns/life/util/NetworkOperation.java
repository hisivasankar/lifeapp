package com.theunknowns.life.util;

import android.content.ContentValues;
import android.net.Uri;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by I308944 on 6/7/2015.
 */
public class NetworkOperation {
    private final static String LOG_TAG = NetworkOperation.class.getSimpleName();
    private final static int ONE_MINUTUE_TIMEOUT = 1 * 60 * 1000;
    private String url, method;
    private int connTimeout, readTimeout;
    private ContentValues params;
    InputStream is;
    public NetworkOperation(String url, ContentValues params, String method, int connectionTimeOut, int readTimeOut) {
        this.url = url;
        this.params = params;
        this.method = method;
        this.connTimeout = connectionTimeOut;
        this.readTimeout = readTimeOut;
    }
    public String makeCall() {
        try {
            URL url = new URL(CustomURLBuilder());
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod(method);
            connection.setConnectTimeout(ONE_MINUTUE_TIMEOUT);
            connection.setReadTimeout(ONE_MINUTUE_TIMEOUT);
            String resp = null;
            connection.connect();
            is = connection.getInputStream();
            int responseCode = connection.getResponseCode();
            Log.i(LOG_TAG, " Connection : Response Code : " + responseCode + "\t" + url.toString());
            resp = readData(is);
            Log.i(LOG_TAG, "Response Data : " + resp);
            return resp;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        return null;
    }
    private String CustomURLBuilder() {
        StringBuilder queryString = new StringBuilder(url).append("?");
        for(String key : params.keySet()) {
            queryString.append(key + "=");
            queryString.append(Uri.encode(params.getAsString(key), "URF-8"));
            queryString.append("&");
        }
        if(queryString.lastIndexOf("&") == queryString.length() -1) {
            return queryString.substring(0, queryString.length() -1).toString();
        } else {
            return queryString.toString();
        }
    }
    private String readData(InputStream inputStream) throws IOException {
        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder data = new StringBuilder();
            String line = "";
            while( (line = br.readLine()) != null ) {
                data.append(line);
            }
            return data.toString();
        } finally {
            if(br != null) {
                try {
                    br.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
}
