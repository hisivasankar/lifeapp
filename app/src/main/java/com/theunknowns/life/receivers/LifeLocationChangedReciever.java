package com.theunknowns.life.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.theunknowns.life.services.LifeLocationService;
import com.theunknowns.life.settings.LifeAppRuntime;

/**
 * Created by I308944 on 6/3/2015.
 */
public class LifeLocationChangedReciever extends BroadcastReceiver{
    private final static String LOG_TAG = LifeLocationChangedReciever.class.getSimpleName();
    private LifeAppRuntime appRuntime;
    @Override
    public void onReceive(Context context, Intent intent) {
        appRuntime = LifeAppRuntime.getInstance(context);
        if(appRuntime.isGPSEnabled() && appRuntime.isAccountsConfigured() && appRuntime.isNetworkEnabled()) {
            context.startService(new Intent(context, LifeLocationService.class));
            Log.i(LOG_TAG, "GPS/Network is enabled");
        } else {
            Log.i(LOG_TAG, "GPS/Network is disabled");
            context.stopService(new Intent(context, LifeLocationService.class));
        }
    }
}
