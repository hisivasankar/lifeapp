package com.theunknowns.life.services;

import android.app.Service;
import android.content.ContentValues;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.theunknowns.life.util.NetworkOperation;
import com.theunknowns.life.settings.LifeAppRuntime;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by I308944 on 6/3/2015.
 */
public class LifeLocationService extends Service {

    private final static String LOG_TAG = LifeLocationService.class.getSimpleName();
    private GoogleApiClient gsAPIClient;
    private LocationRequest locationRequest;
    private LocationListener locationListener;

    private final static int LOCATION_UPDATE_INTERVAL = 1 * 60 * 1000;

    private LifeAppRuntime appRuntime;

    private Looper mLooper;
    private LocationHandler mHandler;
    public LifeLocationService() {

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        Log.i(LOG_TAG, "Location Service already started");
        if(!appRuntime.isAccountsConfigured() || !appRuntime.isNetworkEnabled() || !appRuntime.isGPSEnabled()) {
            stopSelf();
        }
        return START_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(LOG_TAG, "In onCreate() :--");
        appRuntime = LifeAppRuntime.getInstance(getApplicationContext());
        HandlerThread updateServerThread = new HandlerThread("LocationUpdater", Process.THREAD_PRIORITY_BACKGROUND);
        updateServerThread.start();
        mLooper = updateServerThread.getLooper();
        mHandler = new LocationHandler(mLooper);
        initGooglePlayService();

    }
    private void initGooglePlayService() {
        gsAPIClient = new GoogleApiClient.Builder(this)
                .addApiIfAvailable(LocationServices.API)
                .addConnectionCallbacks(new GPConnectionCallback())
                .addOnConnectionFailedListener(new GPConnectionFailureListener())
                .build();

        locationRequest = new LocationRequest()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(LOCATION_UPDATE_INTERVAL)
                .setFastestInterval(LOCATION_UPDATE_INTERVAL);

        locationListener = new LifeLocationListener();
        gsAPIClient.connect();
        Log.i(LOG_TAG, "Google play service and Location service initialized");
    }
    private class LocationHandler extends Handler {
        public LocationHandler(Looper looper) {
            super(looper);
        }
        private String readData(InputStream inputStream) throws IOException{
            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder data = new StringBuilder();
            String line = "";
            while( (line = br.readLine()) != null ) {
                data.append(line);
            }
            return data.toString();
        }
        @Override
        public void handleMessage(Message msg) {
            Bundle data = msg.getData();
            if(msg.obj != null) {
                Location loc = (Location) msg.obj;
                appRuntime.setLastKnowLocation(loc);
                Location t = appRuntime.getLastKnowLocation();
                double latitude = loc.getLatitude();
                double longitude = loc.getLongitude();
                String locationUpdateAPI = appRuntime.getServerRESTAPIEndPoint() + "UpdateUserLocation";
                ContentValues params = new ContentValues();
                params.put("donor_mobile", appRuntime.getDonorMobile());
                params.put("latitude", latitude);
                params.put("longitude", longitude);
                try {
                    NetworkOperation networkOperation = new NetworkOperation(locationUpdateAPI, params, "GET", 5000, 5000);
                    String resp = networkOperation.makeCall();
                    if(resp != null) {
                        Log.i(LOG_TAG, resp);
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            } else {
                Log.i("FROM Thread : ", "No Data found");
            }
        }
    }
    private class LocationUpdater extends Thread{
        public Handler handler;
        String msg;
        Location loc;
        LocationUpdater(String msg) {
            this.msg = msg;
            handler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    super.handleMessage(msg);
                    Bundle data = msg.getData();
                    if(data != null) {
                        Log.i("FROM Thread : " + Thread.currentThread().getId(), data.getString("location"));
                    } else {
                        Log.i("FROM Thread : " + Thread.currentThread().getId(), "No Data found");
                    }
                }
            };
        }

        public Handler getHandler() {
            return handler;
        }
        @Override
        public void run() {
            super.run();
            Log.i("Thread manual", "Thread started");
            Looper.prepare();
            Looper.loop();
        }
    }
    private class LifeLocationListener implements LocationListener {
        @Override
        public void onLocationChanged(Location location) {
            //Log.i(LOG_TAG, "Location :" + location);
            if(appRuntime.isAccountsConfigured()) {
                Bundle args = new Bundle();
                args.putString("location", location.toString());
                Message msg = new Message();
                msg.obj = location;
                msg.setData(args);
                mHandler.sendMessage(msg);
            }
        }
    }
    private class GPConnectionCallback implements GoogleApiClient.ConnectionCallbacks {
        @Override
        public void onConnected(Bundle bundle) {
            Location location = LocationServices.FusedLocationApi.getLastLocation(gsAPIClient);
            LocationServices.FusedLocationApi.requestLocationUpdates(gsAPIClient, locationRequest, locationListener);
            if(location != null) {
            } else {
                Log.i(LOG_TAG, "Location is null");
            }
        }
        @Override
        public void onConnectionSuspended(int i) {
            Log.i(LOG_TAG, "Google play service connection suspended");
        }
    }
    private class GPConnectionFailureListener implements GoogleApiClient.OnConnectionFailedListener {
        @Override
        public void onConnectionFailed(ConnectionResult connectionResult) {
            Log.i(LOG_TAG, "Google play service connection failed");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(LOG_TAG, "Stopping Service");
        if(gsAPIClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(gsAPIClient, locationListener);
        }

    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
