package com.theunknowns.life.services;

import android.content.Intent;
import android.util.Log;

import com.google.android.gms.iid.InstanceIDListenerService;
import com.theunknowns.life.settings.LifeAppRuntime;

/**
 * Created by I308944 on 6/11/2015.
 */
public class LifeInstanceIDService extends InstanceIDListenerService {
    private final static String LOG_TAG = LifeInstanceIDService.class.getSimpleName();

    private LifeAppRuntime appRuntime;
    @Override
    public void onDestroy() {
        Log.i(LOG_TAG, "onDestroy() -- Killing service : ");
        stopSelf();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onCreate() {
        try {
           // String newToken = InstanceID.getInstance(getApplicationContext()).getToken(MainActivity.SENDER_ID, GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
            appRuntime = LifeAppRuntime.getInstance(getApplicationContext());
            appRuntime.updateGCMTokenInAPPServer();
            Log.i(LOG_TAG, "onCreate() -- New token recieved : ");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTokenRefresh() {
        try {
           // String newToken = InstanceID.getInstance(getApplicationContext()).getToken(MainActivity.SENDER_ID, GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
            appRuntime = LifeAppRuntime.getInstance(getApplicationContext());
            appRuntime.updateGCMTokenInAPPServer();
            Log.i(LOG_TAG, "onTokenRefresh() -- New token recieved : ");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
