package com.theunknowns.life.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;
import com.theunknowns.life.activity.FloatingDonorProfile;
import com.theunknowns.life.util.StaticStrings;
import com.theunknows.life.life.R;

import java.util.Random;

/**
 * Created by I308944 on 6/11/2015.
 */
public class LifeGCMService extends GcmListenerService {
    private final static String LOG_TAG = LifeGCMService.class.getSimpleName();
    private final static int NOTIFICATION_ID = 1;
    private NotificationManager notificationManager;
    private NotificationCompat.Builder notificaBuilder;

    public LifeGCMService() {
        super();
    }

    @Override
    public void onMessageSent(String msgId) {
        super.onMessageSent(msgId);
    }

    @Override
    public void onSendError(String msgId, String error) {
        super.onSendError(msgId, error);
    }

    @Override
    public void onDeletedMessages() {
        super.onDeletedMessages();
    }

    @Override
    public void onMessageReceived(String from, Bundle data) {
        String msg = data.getString("data");
        Log.i(LOG_TAG, "Message Recieved Data : " + data);
        createNotification(data);
    }
    private void createNotification(Bundle data) {
        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificaBuilder = new NotificationCompat.Builder(this);

        String name = data.getString(StaticStrings.DONOR_NAME);
        String blood = data.getString(StaticStrings.REQUESTED_BLOOD_GROUP);
        String msg = name + " has request for " + blood + " Blood Group. Click to view the profile";


        Intent profile = new Intent(this, FloatingDonorProfile.class);
        profile.putExtra(StaticStrings.BROADCAST_REQUEST, data);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(FloatingDonorProfile.class);
        stackBuilder.addNextIntent(profile);

        PendingIntent profilePendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        notificaBuilder.setContentIntent(profilePendingIntent);
        notificaBuilder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.icon_launcher));
        notificaBuilder.setSmallIcon(R.drawable.ic_notification);
        notificaBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(msg));
        notificaBuilder.setContentTitle("Life+");
        notificaBuilder.setContentText(msg);
        notificaBuilder.setLights(R.color.ColorPrimaryDark, 5, 5);
        notificaBuilder.setAutoCancel(true);
        notificaBuilder.setSound(RingtoneManager.getActualDefaultRingtoneUri(getApplicationContext(), RingtoneManager.TYPE_NOTIFICATION));

        notificationManager.notify(new Random().nextInt(), notificaBuilder.build());
    }
}
