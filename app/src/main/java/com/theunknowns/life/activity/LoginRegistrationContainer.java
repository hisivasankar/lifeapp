package com.theunknowns.life.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.theunknowns.life.settings.LifeAppRuntime;
import com.theunknows.life.life.R;


public class LoginRegistrationContainer extends AppCompatActivity {
    private LifeAppRuntime appRuntime;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appRuntime = LifeAppRuntime.getInstance(getApplicationContext());

        if (appRuntime.isAccountsConfigured()) {
            Intent mainActivityIntent = new Intent(getApplicationContext(), Home.class);
            mainActivityIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            mainActivityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(mainActivityIntent);
            finish();
        } else {
            setContentView(R.layout.activity_login_registation_container);
            Toolbar appbar = (Toolbar) findViewById(R.id.appbar);
            setSupportActionBar(appbar);
        }
    }

    @Override
    public void onBackPressed() {
        if(getFragmentManager().popBackStackImmediate() == false) {
            finish();
        }
    }
}
