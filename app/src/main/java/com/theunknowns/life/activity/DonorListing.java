package com.theunknowns.life.activity;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.theunknowns.life.fragments.DonorProfile;
import com.theunknowns.life.util.StaticStrings;
import com.theunknows.life.life.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

public class DonorListing extends AppCompatActivity {
    public static String LOG_TAG = DonorListing.class.getSimpleName();
    private Toolbar appbar;

    @Override
    public void onBackPressed() {
        if(getFragmentManager().popBackStackImmediate() == false) {
            super.onBackPressed();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_donor_listing);

        appbar = (Toolbar) findViewById(R.id.appbar);
        setSupportActionBar(appbar);

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        private RecyclerView mRecyclerView;
        private RecyclerView.LayoutManager mLayoutManager;
        private DonorListingAdapter mAdapter;
        private String listingMetadata;

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_donor_listing, container, false);
            Context context = getActivity().getApplicationContext();
            Toolbar appbar = (Toolbar) getActivity().findViewById(R.id.appbar);
            if(appbar.getVisibility() == View.GONE) {
                appbar.setVisibility(View.VISIBLE);
            }
            try {
                String donors_data = getActivity().getIntent().getStringExtra("donors_list");
                JSONArray donors_array = new JSONArray(donors_data);
                mRecyclerView = (RecyclerView) rootView.findViewById(R.id.my_recycler_view);
                mLayoutManager = new LinearLayoutManager(context);
                mAdapter = new DonorListingAdapter(donors_array);
                mRecyclerView.setAdapter(mAdapter);
                mAdapter.setOnItemClickListener(new DonorListingAdapter.OnItemClickListener() {
                    @Override
                    public void onClick(JSONObject user) {
                        Bundle donor_info = new Bundle();
                        Iterator<String> iterator = user.keys();
                        while(iterator.hasNext()) {
                            try {
                                String key = (String) iterator.next();
                                donor_info.putString(key, user.getString(key));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        FragmentManager fm = getFragmentManager();
                        android.app.FragmentTransaction ft = fm.beginTransaction();
                        DonorProfile dp = new DonorProfile();
                        dp.setArguments(donor_info);
                        ft.replace(R.id.container, dp);
                        ft.addToBackStack(null);
                        ft.commit();
                    }
                });
                mRecyclerView.setLayoutManager(mLayoutManager);
            } catch (Exception ex) {
                Log.e(LOG_TAG, "Error : " + ex);
                Toast.makeText(getActivity(), "Failed to load listing.", Toast.LENGTH_SHORT);
            }
            return rootView;
        }
    }
}

class DonorListingAdapter extends RecyclerView.Adapter<DonorListingAdapter.ViewHolder> {
    static JSONArray mDataset;
    OnItemClickListener mListener;

    public  static interface OnItemClickListener {
        void onClick(JSONObject userInfo);
    }

    public DonorListingAdapter(JSONArray data) {
        mDataset = data;
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mListener = listener;
    }

    @Override
    public DonorListingAdapter.ViewHolder onCreateViewHolder(final ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.donor_list_item, parent, false);
        ViewHolder vh = new ViewHolder(v, mListener);
        return vh;
    }

    @Override
    public void onBindViewHolder(DonorListingAdapter.ViewHolder viewHolder, int i) {
        TextView textView_DonorName = (TextView) viewHolder.mView.findViewById(R.id.donorName);
        TextView textView_DonorCity = (TextView) viewHolder.mView.findViewById(R.id.location);
        ImageView mImage = (ImageView) viewHolder.mView.findViewById(R.id.profPic);
        mImage.setImageResource(R.drawable.user_icon);
        try {
            JSONObject user = (JSONObject) mDataset.get(i);
            String username = user.getString(StaticStrings.DONOR_NAME);
            String city = user.getString(StaticStrings.DONOR_CITY);
            textView_DonorName.setText(username);
            textView_DonorCity.setText(city);
        } catch (Exception ex) {
            Log.e(DonorListing.LOG_TAG, "Error:" + ex.getMessage());
        }
    }

    @Override
    public int getItemCount() {
        return mDataset.length();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        View mView;
        OnItemClickListener mListener;
        public ViewHolder(View itemView, OnItemClickListener listener) {
            super(itemView);
            mView = itemView;
            mView.setOnClickListener(this);
            mListener = listener;
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            try {
                mListener.onClick((JSONObject)mDataset.get(position));
            } catch (Exception ex) {
                Log.e(DonorListing.LOG_TAG, "Error : " + ex.getMessage());
            }
        }
    }
}
