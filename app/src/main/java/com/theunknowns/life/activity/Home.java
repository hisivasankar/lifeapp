package com.theunknowns.life.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.theunknowns.life.fragments.BloodGroupSearch;
import com.theunknowns.life.fragments.MapsSearch;
import com.theunknowns.life.fragments.Settings;
import com.theunknowns.life.services.LifeLocationService;
import com.theunknowns.life.settings.LifeAppRuntime;
import com.theunknowns.life.tabs.SlidingTabLayout;
import com.theunknows.life.life.R;


public class Home extends AppCompatActivity {
    private final static String LOG_TAG = Home.class.getSimpleName();
    private ViewPager mViewPager;
    private LifePagerAdapter mPagerAdapter;
    private Toolbar mAppbar;
    private SlidingTabLayout mTabs;
    private LifeAppRuntime appRuntime;

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        appRuntime = LifeAppRuntime.getInstance(this);

        mAppbar = (Toolbar) findViewById(R.id.appbar);
        mTabs = (SlidingTabLayout) findViewById(R.id.tabs);
        mViewPager = (ViewPager) findViewById(R.id.pager);

        setSupportActionBar(mAppbar);

        String[] headers = getResources().getStringArray(R.array.TabsList);
        mPagerAdapter = new LifePagerAdapter(getSupportFragmentManager(), headers, headers.length);
        mViewPager.setAdapter(mPagerAdapter);

        mTabs.setDistributeEvenly(true);
        mTabs.setViewPager(mViewPager);
        mTabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.TabScrollSelector);
            }
        });



        /*actionBar.selectTab(actionBar.getTabAt(1));
        mViewPager.setCurrentItem(1);*/
        if(!appRuntime.isNetworkEnabled()) {
            Toast.makeText(this, "Please connect to network.", Toast.LENGTH_SHORT).show();
        }
        if(appRuntime.isGPSEnabled()) {
            startService(new Intent(this, LifeLocationService.class));
        }
        if(savedInstanceState != null) {
            onRestoreInstanceState(savedInstanceState);
        }
        if(checkPlayService()) {
            appRuntime.initializePushNotification();
        }
    }
    private boolean checkPlayService() {
        final int GOOGLEPLAY_SERVICE_REQUEST_CODE = 5;
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if(resultCode != ConnectionResult.SUCCESS) {
            if(GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this, GOOGLEPLAY_SERVICE_REQUEST_CODE).show();
            } else {
                Log.e(LOG_TAG, "Device not supported");
                finish();
            }
            return false;
        }
        return true;
    }
    public class LifePagerAdapter extends FragmentPagerAdapter {
        private String[] tabTitles;
        private int noOfTabs;
        public LifePagerAdapter(FragmentManager fm, String[] tabHeaders, int tabCount) {
            super(fm);
            tabTitles = tabHeaders;
            noOfTabs = tabCount;
        }
        @Override
        public Fragment getItem(int pos) {
            switch (pos) {
                case 0:
                    return new BloodGroupSearch();
                case 1:
                    return new MapsSearch();
                default:
                    return new Settings();
            }
        }
        @Override
        public int getCount() {
            return noOfTabs;
        }
        @Override
        public CharSequence getPageTitle(int position) {
            return tabTitles[position];
        }
    }
}
